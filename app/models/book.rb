class Book < ActiveRecord::Base

  belongs_to :owner, class_name: User.name, counter_cache: true
  belongs_to :catalogue, counter_cache: true

  attr_accessor :query

  def self.search(query)
    client = Goodreads.new
    client.search_books(query)
  end
end
