
class Catalogue < ActiveRecord::Base
  validates_uniqueness_of :ISBN
  has_many :books
end
