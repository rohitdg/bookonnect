class UsersController < ApplicationController
  before_action :authenticate_user!
  def new
    @user = User.new
  end

  def index
    @users = User.all.paginate(page: params[:page], per_page: 12)
  end

  def edit
    @user = current_user
  end


  def show
    @hide_searchbar = true
    @user  = User.find(params[:id])
    @cat_ids = @user.books.pluck(:catalogue_id)
    @cat = Catalogue.where(id: @cat_ids)
  end

  def collection
    @hide_searchbar = true
    @user  = current_user

    @cat_ids = @user.books.pluck(:catalogue_id)
    @cat = Catalogue.where(id: @cat_ids)
  end

  def update
    byebug
    @user = User.find(params[:id])
    @user.update_attributes(clean_params)
  end

  private
  def clean_params
    params.require(:user).permit(:name, :email, :department, :course, :yearofjoining, :hostel)
  end

  def edit
    
  end

end