class CataloguesController < ApplicationController
  before_action :authenticate_user!

  def index
    @catalogue = Catalogue.new
    @catalogues = Catalogue.all
  end

  def show
    @hide_searchbar = true
    @catalogue = Catalogue.find(params[:id])
    @user_id = Book.where(catalogue_id: @catalogue.id).select("owner_id")
    @users = User.where(id: @user_id)
  end
end
