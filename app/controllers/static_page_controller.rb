class StaticPageController < ApplicationController
  def home
    @hide_header = true
    @hide_searchbar = true
  end
end
