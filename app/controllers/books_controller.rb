class BooksController < ApplicationController

  before_action :authenticate_user!


  def new
    @book = Book.new
  end

  def index
    @cat = Catalogue.where("books_count > 0")
    @books = @cat.paginate(page: params[:page], per_page: 24)
  end

  def show

  end

  def search
    @book_results = Book.search(params[:query])
    if @book_results.total_results == "0" || @book_results.nil?
      redirect_to new_book_path(no_results: true)
    end
  end

  def destroy
    @book = Book.find(params[:id])
    @book.destroy
    redirect_to collection_path
  end

  def create
    @goodreads_id = params[:goodreads_id]
    @title = params[:title]
    gclient = Goodreads.new
    gbook = gclient.book_by_title(@title)
    @catalogue = Catalogue.new
    @catalogue.title = gbook.title
    @catalogue.ISBN = gbook.isbn
    @catalogue.pages = gbook.num_pages
    @catalogue.author = gbook.authors.author.name
    if gbook.image_url.nil?
      @catalogue.photo = "https://content.redshelf.com/site_media/media/cover_image/default_book_cover.jpg"
    else
      @catalogue.photo = gbook.image_url
    end

    @book = current_user.books.new
    @book.catalogue = @catalogue
    @catalogue.save
    if @book.save
      flash.notice = "#{gbook.title} has been added to your books"
    end
  end
end
