class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.references :catalogue
      t.references :owner
      t.boolean :borrowed

      t.timestamps
    end
  end
end
