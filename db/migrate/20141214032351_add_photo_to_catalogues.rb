class AddPhotoToCatalogues < ActiveRecord::Migration
  def change
    add_column :catalogues, :photo, :string
  end
end
