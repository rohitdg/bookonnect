class AddBooksCountToCatalogues < ActiveRecord::Migration
  def change
    add_column :catalogues, :books_count, :integer
  end
end
