class CreateCatalogues < ActiveRecord::Migration
  def change
    create_table :catalogues do |t|
      t.string :ISBN
      t.string :title
      t.string :author
      t.string :language
      t.string :pages
      t.string :genre

      t.timestamps
    end
  end
end
