class AddDetailsToUser < ActiveRecord::Migration
  def change
    add_column :users, :department, :string
    add_column :users, :course, :string
    add_column :users, :yearofjoining, :integer
    add_column :users, :hostel, :string

  end
end
