# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

hostelArray = ["Pampa", "Jam", "Ganga", "Mandak", "Sharav", "Brahms", "Godav", "Saras", "Alak","Narmad","Sarayu","Sabarmati", "Other"]
departmentArray = ["Mech", "Aero", "BT", "Chem", "Civil", "CS", "Elec", "Meta", "Nav Arch", "ED", "Maths", "Chemistry", "EP", "DoMS", "MA", "Other"]
courseArray = ["B. Tech", "Dual Degree", "M Tech", "M. Sc.", "Ph. D", "MBA", "Other"]
yearofjoiningArray = (2006..2014).to_a

100.times do
 name = Faker::Name.name
 email = Faker::Internet.email
 photo = Faker::Avatar.image
 usr = User.create!(name: name, hostel: hostelArray.sample,
                    department: departmentArray.sample, course: courseArray.sample,
                    yearofjoining: yearofjoiningArray.sample,
                    email: email, password: "password123",
                    password_confirmation: "password123",
                    photo: photo)
 usr.confirm!
end

genreArray = [
"Art",
"Biography",
"Business",
"Chick Lit",
"Children's",
"Christian",
"Classics",
"Comics",
"Contemporary",
"Cookbooks",
"Crime",
"Ebooks",
"Fantasy",
"Fiction",
"Gay And Lesbian",
"Graphic Novels",
"Historical Fiction",
"History",
"Horror",
"Humor And Comedy",
"Manga",
"Memoir",
"Music",
"Mystery",
"Non Fiction",
"Paranormal",
"Philosophy",
"Poetry",
"Psychology",
"Religion",
"Romance",
"Science",
"Science Fiction",
"Self Help",
"Suspense",
"Spirituality",
"Sports",
"Thriller",
"Travel",
"Young Adult"]

languageArray = ["English","Telugu","Tamil","Kannada","Malayalam","French","Greek","Latin","Hindi","Bengali","Latin","Greek"]

100.times do
  isbn = Faker::Code.isbn
  title = Faker::Name.title
  author = Faker::Name.name
  pages = Faker::Number.number(3)
  photo = Faker::Avatar.image
  Catalogue.create!(ISBN: isbn, title: title, author: author,
    language: languageArray.sample, pages: pages , genre: genreArray.sample , photo: photo)
end

users =User.all
catalogues = Catalogue.all
boolValues = ["true","false"]

100.times do 
  Book.create!(catalogue_id: catalogues.sample.id, owner_id: users.sample.id, borrowed: boolValues.sample)
end  
